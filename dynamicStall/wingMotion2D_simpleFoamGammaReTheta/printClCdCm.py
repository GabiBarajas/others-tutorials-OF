# importing re module for creating

# regex expression

import re
import numpy as np
from io import StringIO
import matplotlib.pyplot as plt


fileNameForce = "postProcessing/force.dat"
fileNameMoment = "postProcessing/moment.dat"

fileNameForce1 = "postProcessing/forces1/0/force.dat"
fileNameMoment1 = "postProcessing/forces1/0/moment.dat"


amplitude = 15.0
omega = 0.27
alphaStart = 10.0

Ux = 1.33
Uz = 0.23
UmagSq = Ux*Ux + Uz*Uz
L = 1
H = 0.05



def FPvectorReader(fileName):

    #with open(fileName, 'r') as f:
              ## looping the para and iterating
              ## each line
        #text = f.read()
              ## getting the pattern for [],(),{}
              ## brackets and replace them to empty
              ## string
              ## creating the regex pattern & use re.sub()
        #patn = re.sub(r"[\([{})\]]", "", text)
        
        ##print(patn)

 
    array = np.genfromtxt(fileName,comments="#")
    
    
    tarray=  (array[:,0])
    FxTot =  (array[:,1])
    FyTot =  (array[:,2])
    FzTot =  (array[:,3])
    FxPre =  (array[:,4])
    FyPre =  (array[:,5])
    FzPre =  (array[:,6])
    FxVis =  (array[:,7])
    FyVis =  (array[:,8])
    FzVis =  (array[:,9])

    return tarray,FxTot,FyTot,FzTot,FxPre,FyPre,FzPre,FxVis,FyVis,FzVis





if __name__ == "__main__":


    tarray,FxTot,FyTot,FzTot,FxPre,FyPre,FzPre,FxVis,FyVis,FzVis = FPvectorReader(fileNameForce)
    
    tarray1,MxTot,MyTot,MzTot,MxPre,MyPre,MzPre,MxVis,MyVis,MzVis = FPvectorReader(fileNameMoment)
    
    Fptot = np.sqrt(np.square(FzPre),np.square(FxPre))
    
    alpharad = alphaStart*np.pi/180
    
    Fdrag = np.subtract(np.multiply(FxPre,
            np.cos(-alpharad)),np.multiply(FzPre,np.sin(-alpharad)))
    Flift = np.add(np.multiply(FxPre,
            np.sin(-alpharad)),np.multiply(FzPre,np.cos(-alpharad)))
    
    Cl = 2*Flift/H/L/UmagSq
    Cd = 2*Fdrag/H/L/UmagSq
    Cm = 2*MyPre/H/L/UmagSq

    print "Cl = "+str(Cl[-1])
    print "Cd = "+str(Cd[-1])
    print "Cm = "+str(Cm[-1])



 


