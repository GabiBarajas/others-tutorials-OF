/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2019-2021 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::regionFaModels::isotropicPrestressMembraneModel

Description

Usage
    Example of the boundary condition specification:
    \verbatim
    <patchName>
    {
        // Mandatory/Optional entries
        ...

        // Mandatory entries
        vibrationShellModel   isotropicPrestressMembraneModel;
        f0                    0.04;
        f1                    0.0;
        f2                    0.0;
    }
    \endverbatim

    where the entries mean:
    \table
      Property   | Description                         | Type  | Reqd | Deflt
      vibrationShellModel | Type name: isotropicPrestressMembraneModel  | word  | yes  | -
      f0         | Damping coefficient [1/s]           | scalar | yes | -
      f1         | Damping coefficient [1/s]           | scalar | yes | -
      f2         | Damping coefficient [1/s]           | scalar | yes | -
    \endtable

    The inherited entries are elaborated in:
      - \link vibrationShellModel.H \endlink

SourceFiles
    isotropicPrestressMembraneModel.C

\*---------------------------------------------------------------------------*/

#ifndef Foam_regionModels_isotropicPrestressMembraneModel_H
#define Foam_regionModels_isotropicPrestressMembraneModel_H

#include "volFieldsFwd.H"
#include "membraneModel.H"
#include "faMesh.H"
#include "faCFD.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace regionModels
{

/*---------------------------------------------------------------------------*\
                        Class isotropicPrestressMembraneModel Declaration
\*---------------------------------------------------------------------------*/

class isotropicPrestressMembraneModel
:
    public membraneModel
{
    // Private Data

        //- pre tension [dimpressure]
        dimensionedScalar sigma0_;


    // Private Member Functions

        //- Initialise isotropicPrestressMembraneModel
        bool init(const dictionary& dict);


protected:

    // Protected Data

        // Solution parameters

            //- Number of non orthogonal correctors
            label nNonOrthCorr_;

        // Source term fields

            //- External surface source [Pa]
            areaScalarField ps_;

            //- Thickness [m]
            areaScalarField h_;
            
            // laplacian of the undeformed configuration
            const faScalarMatrix laplaceUndeformed_;




    // Protected Member Functions

        // Equations

            //- Solve energy equation
            void solveDisplacement();


public:

    //- Runtime type information
    TypeName("isotropicPrestressMembraneModel");


    // Constructors

        //- Construct from components and dict
        isotropicPrestressMembraneModel
        (
            const word& modelType,
            const fvMesh& mesh,
            const dictionary& dict
        );

        //- No copy construct
        isotropicPrestressMembraneModel(const isotropicPrestressMembraneModel&) = delete;

        //- No copy assignment
        void operator=(const isotropicPrestressMembraneModel&) = delete;


    //- Destructor
    virtual ~isotropicPrestressMembraneModel() = default;


    // Member Functions

        // Fields

            //- Return stiffness
            const tmp<areaScalarField> D() const;

            //- Return density [Kg/m3]
            const tmp<areaScalarField> rho() const;


        // Evolution

            //- Pre-evolve  thermal baffle
            virtual void preEvolveRegion();

            //- Evolve the thermal baffle
            virtual void evolveRegion();


       // IO

            //- Provide some feedback
            virtual void info();
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace regionModels
} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


#endif

// ************************************************************************* //
