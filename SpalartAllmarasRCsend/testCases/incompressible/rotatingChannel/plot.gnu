set term 'png'
set output 'UrotChannel.png'
set xl 'x/H'
set yl 'U/Um'


plot 'SpalartAllmaras/postProcessing/graphs/40000/line1_U.xy' u 1:(1.*($2+$1*0.5)) w l tit 'SA' lw 3 ,   'SpalartAllmarasRC/postProcessing/graphs/40000/line1_U.xy' u 1:(1.*($2+$1*0.5)) w l tit 'SARC' lw 3

set output 'nutRotchannel.png
set yl 'nut/nu'

plot 'SpalartAllmaras/postProcessing/graphs/40000/line1_nuTilda_nut.xy' u 1:($3/1.724e-04) w l tit 'SA' lw 3,  'SpalartAllmarasRC/postProcessing/graphs/40000/line1_nuTilda_nut.xy' u 1:($3/1.724e-04) w l tit 'SARC' lw 3


set output 'fr1RotatingChannel.png'
set yl 'fr1'

plot 'SpalartAllmarasRC/postProcessing/graphs/40000/line1_fr1_nuTilda_nut.xy' u 1:2 w l tit 'SARC' lw 3,  'SpalartAllmarasRCOmega0/postProcessing/graphs/40000/line1_fr1_nuTilda_nut.xy' u 1:2 w l tit 'SARC omega = 0' lw 3 lc 7
