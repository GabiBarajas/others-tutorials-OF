set xr [-5:15]
set xl 'S/H'
set yl 'cf'
set term png
set output 'cfUturnOuterWall.png'

plot 'SpalartAllmarasRC/postProcessing/samplePouterWall1/surface/5000/wallShearStress_patch_outerWall1.raw' u 1:(-$4*2) w l lw 2  lt 2 lc 2 notitle, 'SpalartAllmarasRC/postProcessing/samplePouterWall2/surface/5000/wallShearStress_patch_outerWall2.raw' u (acos(-$2/1.5)):(2*($4 + $5)) w l lw 2  lt 2 lc 2 notitle, 'SpalartAllmarasRC/postProcessing/samplePouterWall3/surface/5000/wallShearStress_patch_outerWall3.raw' u (acos(-$2/1.5)):(2*($4 + $5))  w l lw 2  lt 2 lc 2 notitle, 'SpalartAllmarasRC/postProcessing/samplePouterWall4/surface/5000/wallShearStress_patch_outerWall4.raw' u (pi -$1):(2*$4)  w l lw 2  lt 2 lc 2 tit 'SARC', 'Cf_outer_wall.dat' u 1:2 w p  pt 6 lw 2 lc rgb "red" tit 'exp' ,  'SpalartAllmaras/postProcessing/samplePouterWall1/surface/5000/wallShearStress_patch_outerWall1.raw' u 1:(-$4*2) w l lw 2  lt 2 lc 1 notitle, 'SpalartAllmaras/postProcessing/samplePouterWall2/surface/5000/wallShearStress_patch_outerWall2.raw' u (acos(-$2/1.5)):(2*($4 + $5)) w l lw 2  lt 2 lc 1 notitle, 'SpalartAllmaras/postProcessing/samplePouterWall3/surface/5000/wallShearStress_patch_outerWall3.raw' u (acos(-$2/1.5)):(2*($4 + $5))  w l lw 2  lt 2 lc 1 notitle, 'SpalartAllmaras/postProcessing/samplePouterWall4/surface/5000/wallShearStress_patch_outerWall4.raw' u (pi -$1):(2*$4)  w l lw 2  lt 2 lc 1 tit 'SA'


set output 'cpUturnOuterWall.png'
set yl 'cp'

plot 'SpalartAllmarasRC/postProcessing/samplePouterWall1/surface/5000/p_patch_outerWall1.raw' u 1:(2*($4- 0.22)) w l lw 2  lt 2 lc 2 notitle, 'SpalartAllmarasRC/postProcessing/samplePouterWall2/surface/5000/p_patch_outerWall2.raw' u (acos(-$2/1.5)):(2*($4- 0.22)) w l lw 2  lt 2 lc 2 notitle, 'SpalartAllmarasRC/postProcessing/samplePouterWall3/surface/5000/p_patch_outerWall3.raw' u (acos(-$2/1.5)):(2*($4- 0.22))  w l lw 2  lt 2 lc 2 notitle, 'SpalartAllmarasRC/postProcessing/samplePouterWall4/surface/5000/p_patch_outerWall4.raw' u (pi -$1):($4- 0.22)  w l lw 2  lt 2 lc 2 tit 'SARC', 'Cp_outer_wall.dat' u 1:2 w p  pt 6 lw 2 lc rgb "red" tit 'exp', 'SpalartAllmaras/postProcessing/samplePouterWall1/surface/5000/p_patch_outerWall1.raw' u 1:(2*($4- 0.18)) w l lw 2  lt 2 lc 1 notitle, 'SpalartAllmaras/postProcessing/samplePouterWall2/surface/5000/p_patch_outerWall2.raw' u (acos(-$2/1.5)):(2*($4- 0.18)) w l lw 2  lt 2 lc 1 notitle, 'SpalartAllmaras/postProcessing/samplePouterWall3/surface/5000/p_patch_outerWall3.raw' u (acos(-$2/1.5)):(2*($4- 0.18))  w l lw 2  lt 2 lc 1 notitle, 'SpalartAllmaras/postProcessing/samplePouterWall4/surface/5000/p_patch_outerWall4.raw' u (pi -$1):($4- 0.18)  w l lw 2  lt 2 lc 1 tit 'SA'


set output 'cfUturnInnerWall.png'
set yl 'cf'

plot 'SpalartAllmarasRC/postProcessing/samplePinnerWall1/surface/5000/wallShearStress_patch_innerWall1.raw' u 1:(-$4*2) w l lw 2  lt 2 lc 2 notitle, 'SpalartAllmarasRC/postProcessing/samplePinnerWall2/surface/5000/wallShearStress_patch_innerWall2.raw' u (acos(-$2/0.5)):(2*(-$4 - $5)) w l lw 2  lt 2 lc 2 notitle, 'SpalartAllmarasRC/postProcessing/samplePinnerWall3/surface/5000/wallShearStress_patch_innerWall3.raw' u (acos(-$2/0.5)):(2*($4 + $5))  w l lw 2  lt 2 lc 2 notitle, 'SpalartAllmarasRC/postProcessing/samplePinnerWall4/surface/5000/wallShearStress_patch_innerWall4.raw' u (pi -$1):(2*$4)w l lw 2  lc 2 tit 'SARC', 'Cf_inner_wall.dat' u 1:2 w p  pt 6 lw 2 lc rgb "red" tit 'exp' , 'SpalartAllmaras/postProcessing/samplePinnerWall1/surface/5000/wallShearStress_patch_innerWall1.raw' u 1:(-$4*2) w l lw 2  lt 2 lc 1 notitle, 'SpalartAllmaras/postProcessing/samplePinnerWall2/surface/5000/wallShearStress_patch_innerWall2.raw' u (acos(-$2/0.5)):(2*(-$4 - $5)) w l lw 2  lt 2 lc 1 notitle, 'SpalartAllmaras/postProcessing/samplePinnerWall3/surface/5000/wallShearStress_patch_innerWall3.raw' u (acos(-$2/0.5)):(2*($4 + $5))  w l lw 2  lt 2 lc 1 notitle, 'SpalartAllmaras/postProcessing/samplePinnerWall4/surface/5000/wallShearStress_patch_innerWall4.raw' u (pi -$1):(2*$4)w l lw 2  lt 2 lc 1 tit 'SA'


set output 'cpUturnInnerWall.png'
set yl 'cp'
set key bottom right

plot 'SpalartAllmarasRC/postProcessing/samplePinnerWall1/surface/5000/p_patch_innerWall1.raw' u 1:(2*($4- 0.22)) w l lw 2  lt 2 lc 2 notitle, 'SpalartAllmarasRC/postProcessing/samplePinnerWall2/surface/5000/p_patch_innerWall2.raw' u (acos(-$2/0.5)):(2*($4- 0.22)) w l lw 2  lt 2 lc 2 notitle, 'SpalartAllmarasRC/postProcessing/samplePinnerWall3/surface/5000/p_patch_innerWall3.raw' u (acos(-$2/0.5)):(2*($4- 0.22))  w l lw 2  lt 2 lc 2 notitle, 'SpalartAllmarasRC/postProcessing/samplePinnerWall4/surface/5000/p_patch_innerWall4.raw' u (pi -$1):($4- 0.22)  w l lw 2  lt 2 lc 2 tit 'SARC', 'Cp_inner_wall.dat' u 1:2 w p  pt 6 lw 2 lc rgb "red" tit 'exp' , 'SpalartAllmaras/postProcessing/samplePinnerWall1/surface/5000/p_patch_innerWall1.raw' u 1:(2*($4- 0.18)) w l lw 2  lt 2 lc 1 notitle, 'SpalartAllmaras/postProcessing/samplePinnerWall2/surface/5000/p_patch_innerWall2.raw' u (acos(-$2/0.5)):(2*($4- 0.18)) w l lw 2  lt 2 lc 1 notitle, 'SpalartAllmaras/postProcessing/samplePinnerWall3/surface/5000/p_patch_innerWall3.raw' u (acos(-$2/0.5)):(2*($4- 0.18))  w l lw 2  lt 2 lc 1 notitle, 'SpalartAllmaras/postProcessing/samplePinnerWall4/surface/5000/p_patch_innerWall4.raw' u (pi -$1):($4- 0.18)  w l lw 2  lt 2 lc 1 tit 'SA'


